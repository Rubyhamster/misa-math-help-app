# MISA, A math helping app made for our STEM project
## By team 10720X

##### Our team tirelessly worked for this app to run effectively (special thanks to Braden(@rubyhamster) for helping us learn C# to code the following app). This app was created through 8 in school practices and over 60+ hours at home.  We used the IronOcr library for the character recognition and Xamarin for the mobile app development. Their links can be found here:
#### IronOcr: (https://ironsoftware.com/csharp/ocr/)
#### Xamarin: (https://visualstudio.microsoft.com/xamarin/)
*Created by @rubyhamster (Braden Everson), @henman (Henry Heasty), Yabo Zhou, and Aaron Fasick.*
## Check Out Our Youtube Channel and our Website!
#### Channel: https://www.youtube.com/channel/UC2uNjjB1u-HlbSPTIf-XiIw
#### Website: http://10720x.000webhostapp.com/