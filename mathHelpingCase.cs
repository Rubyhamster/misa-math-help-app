using System;
public class Program
{
    static void solve(string equation)
    {
        int w = 0;
        bool isAddition = false;
        bool isSubtraction = false;
        bool isMultiplication = false;
        bool isDivision = false;
        bool isSquare = false;
        bool isFactorial = false;
        double a = 0;
        if (equation[equation.Length - 1] == '-')
        {
            equation = "0" + equation + "0";
        }
        else
        {
            equation = "0+" + equation + "+0";
        }

        char[] mun = equation.ToCharArray();
        string[] num = new string[mun.Length];
        for (int i = 0; i < mun.Length; i++)
        {
            num[i] = mun[i].ToString().ToLowerInvariant();
        }
             for (int i = 0; i < num.Length; i++)
        {
            try
            {
                if (Int32.TryParse(num[i], out w) && Int32.TryParse(num[i + 1], out w))
                {
                    if (Int32.TryParse(num[i + 1], out w) && Int32.TryParse(num[i + 2], out w))
                    {
                        if (Int32.TryParse(num[i + 2], out w) && Int32.TryParse(num[i + 3], out w))
                        {
                            num[i] = num[i] + num[i + 1] + num[i + 2] + num[i + 3];
                            num[i + 1] = "";
                            num[i + 2] = "";
                            num[i + 3] = "";
                        }
                        else
                        {
                            num[i] = num[i] + num[i + 1] + num[i + 2];
                            num[i + 1] = "";
                            num[i + 2] = "";

                        }
                    }
                    else
                    {
                        num[i] = num[i] + num[i + 1];
                        num[i + 1] = "";
                    }
                    }
                }
            catch { }
        }
        for (int y = 0; y < num.Length; y++)
        {
            if (num[y] == "+" || (num[y] == "/") || (num[y] == "x") || (num[y] == "-") || (num[y] == "!"))
            {
                //do nothing and cry
            }
            else
            {
                try
                {
                    if (num[y - 1] == "!")
                    {
                        int fact = Int32.Parse(num[y]);
                        for (int i = fact - 1; i > 0; i--)
                        {
                            fact *= i;
                        }

                        a += fact;
                    }

                    if (num[y - 1] == "+")
                    {
                        a += Int32.Parse(num[y]);
                    }

                    if (num[y] == "^")
                    {
                        a = Math.Pow(a, Int32.Parse(num[y + 1]));
                    }

                    if (num[y - 1] == "x")
                    {
                        a *= Int32.Parse(num[y]);
                    }

                    if (num[y - 1] == "/")
                    {
                        a /= Int32.Parse(num[y]);
                    }

                    if (num[y - 1] == "-")
                    {
                        a -= Int32.Parse(num[y]);
                    }
                }
                catch
                {
                     try {
                        a += Int32.Parse(num[y]);
                    }
                    catch
                    {

                    }
                }
            }
        }
        
        Console.Write("Answer you got:");
       Help(Int32.Parse(Console.ReadLine()),Int32.Parse(a.ToString()),num);
    }
    static void Help(int a, int answer, String[] num)
    {
        bool isAddition = false;
        bool isSubtraction = false;
        bool isMultiplication = false;
        bool isDivision = false;
        bool isSquare = false;
        bool isFactorial = false;
        int p = 0;
        if (answer == a)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Yes! That's Correct!!!");
            Console.ResetColor();
        }
        else
        {
            for (int u = 0; u < num.Length; u++)
            {
                switch (num[u])
                {
                    case "+":
                        p++;
                        if (p >= 2)
                        {
                            isAddition = true;
                        }
                        break;
                    case "-":
                        isSubtraction = true;
                        break;
                    case "x":
                        isMultiplication = true;
                        break;
                    case "/":
                        isDivision = true;
                        break;
                    case "^":
                        isSquare = true;
                        break;
                    case "!":
                        isFactorial = true;
                        break;
                    default:
                        break;
                }
            }
            if (isAddition)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Oh! Looks like you have trouble with addition. With addition you add the two numbers, so 1+1 would be 2, becasue 1 with another 1 added to it is 2.");
                Console.ResetColor();
            }
            if (isMultiplication)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Oh! Looks like you have trouble with multiplying. With multiplication you add the first number as many times as the second number, so 2x3 becomes 2+2+2, or 6");
                Console.ResetColor();
            }
            if (isDivision)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Oh! Looks like you have trouble with division! With division you see how many times the second number can go into the first one, so 6/3 would be 2, because 3 goes into 6 2 times");
                Console.ResetColor();
            }
            if (isSubtraction)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Oh! Looks like you have trouble with subtraction. With subtraction you take away the second number from the first number, so 2-1 becomes 1, because if you take 1 away from 2 it becomes 1");
                Console.ResetColor();
            }
            if (isSquare)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Oh! Looks like you have trouble with exponents. With exponents you multiply the first number by itself as many times as the second number, so 5^2, it becomes 5x5, or 25");
                Console.ResetColor();
            }
            if (isFactorial)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Oh! Looks like you have trouble with factorials. With factorials you multiply the number by all of the numbers below it, so !5 becomes 5x4x3x2x1, or 120");
                Console.ResetColor();
            }
        }
    }

    public static void Main()
    {
		while(true){
        solve(Console.ReadLine());
		}
    }
}
