using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class Program
{
    static double solve(string equation)
    {
        int w = 0;
        double a = 0;
        try
        {
            if (equation[equation.Length - 1] == '-')
            {
                equation = "0" + equation + "0";
            }
            else
            {
                equation = "0+" + equation + "+0";
            }
        }
        catch
        {
            equation = "0+" + equation + "+0";
        }
        char[] mun = equation.ToCharArray();
        string[] num = new string[mun.Length];

        for (int i = 0; i < mun.Length; i++)
        {
            num[i] = mun[i].ToString().ToLowerInvariant();
        }
        for (int i = 0; i < num.Length; i++)
        {
            try
            {
                if (Int32.TryParse(num[i], out w) && Int32.TryParse(num[i + 1], out w))
                {
                    if (Int32.TryParse(num[i + 1], out w) && Int32.TryParse(num[i + 2], out w))
                    {
                        if (Int32.TryParse(num[i + 2], out w) && Int32.TryParse(num[i + 3], out w))
                        {
                            num[i] = num[i] + num[i + 1] + num[i + 2] + num[i + 3];
                            num[i + 1] = "";
                            num[i + 2] = "";
                            num[i + 3] = "";
                        }
                        else
                        {
                            num[i] = num[i] + num[i + 1] + num[i + 2];
                            num[i + 1] = "";
                            num[i + 2] = "";

                        }
                    }
                    else
                    {
                        num[i] = num[i] + num[i + 1];
                        num[i + 1] = "";
                    }
                }
            }
            catch { }
        }
        for (int y = 0; y < num.Length; y++)
        {
            if (num[y] == "+" || (num[y] == "/") || (num[y] == "x") || (num[y] == "-") || (num[y] == "!"))
            {
                //do nothing and cry
            }
            else
            {
                try
                {
                    if (num[y - 1] == "!")
                    {
                        int fact = Int32.Parse(num[y]);
                        for (int i = fact - 1; i > 0; i--)
                        {
                            fact *= i;
                        }

                        a += fact;
                    }

                    if (num[y - 1] == "+")
                    {
                        a += Int32.Parse(num[y]);
                    }

                    if (num[y] == "^")
                    {
                        a = Math.Pow(a, Int32.Parse(num[y + 1]));
                    }

                    if (num[y - 1] == "x")
                    {
                        a *= Int32.Parse(num[y]);
                    }

                    if (num[y - 1] == "/")
                    {
                        a /= Int32.Parse(num[y]);
                    }

                    if (num[y - 1] == "-")
                    {
                        a -= Int32.Parse(num[y]);
                    }
                }
                catch
                {
                    try
                    {
                        a += Int32.Parse(num[y]);
                    }
                    catch { }
                }
            }
        }
        return a;
    }
    static void splitItUp(string equation)
    {
        int w = 0;
        string eq = "";
        equation += "+0";
        char[] mun = equation.ToCharArray();
        //string[] num = new string[mun.Length];
        List<string> num = new List<string>();
        for (int i = 0; i < mun.Length; i++)
        {
            //num[i] = mun[i].ToString().ToLowerInvariant();
            num.Add(mun[i].ToString());
        }
        for (int i = 0; i < num.Count; i++)
        {
            try
            {
                if (Int32.TryParse(num[i], out w) && Int32.TryParse(num[i + 1], out w))
                {
                    if (Int32.TryParse(num[i + 1], out w) && Int32.TryParse(num[i + 2], out w))
                    {
                        if (Int32.TryParse(num[i + 2], out w) && Int32.TryParse(num[i + 3], out w))
                        {
                            if (Int32.TryParse(num[i + 2], out w) && Int32.TryParse(num[i + 3], out w) && Int32.TryParse(num[i + 4], out w))
                            {
                                num[i] = num[i] + num[i + 1] + num[i + 2] + num[i + 3] + num[i + 4];
                                num.RemoveAt(i + 1);
                                num.RemoveAt(i + 2);
                                num.RemoveAt(i + 3);
                                num.RemoveAt(i + 4);
                            }
                            else
                            {
                                num[i] = num[i] + num[i + 1] + num[i + 2] + num[i + 3] + num[i + 4];
                                num.RemoveAt(i + 1);
                                num.RemoveAt(i + 2);
                                num.RemoveAt(i + 3);
                            }
                        }
                        else
                        {
                            num[i] = num[i] + num[i + 1] + num[i + 2];
                            num.RemoveAt(i + 1);
                            num.RemoveAt(i + 2);

                        }
                    }
                    else
                    {
                        num[i] = num[i] + num[i + 1];
                        num.RemoveAt(i + 1);
                    }
                }
            }
            catch { }
        }
        for (int i = 0; i < num.Count; i++)
        {
            if (num[i].ToString().ToLowerInvariant() == "^")
            {
                if (num.ToString().ToLowerInvariant().Contains("x") && num.ToString().ToLowerInvariant().Contains("/") && !(num[i].ToString().ToLowerInvariant().Contains("-") && num[i].ToString().ToLowerInvariant().Contains("+")))
                {
                    num[i + 1] += "'";
                    num[i - 2] += "'";
                }
                else if (num.ToString().ToLowerInvariant().Contains("+") && num.ToString().ToLowerInvariant().Contains("-") && !(num[i].ToString().ToLowerInvariant().Contains("x") && num[i].ToString().ToLowerInvariant().Contains("-")))
                {
                    num[i + 1] += "'";
                    num[i - 2] += "'";
                }
                else
                {
                    try
                    {
                        num[i + 1] += "'";
                        num[i - 2] += "'";
                    }
                    catch { }
                }
            }
        }
        for (int i = 0; i < num.Count; i++)
        {
            if (num.ToString().ToLowerInvariant().Contains("x") && num.ToString().ToLowerInvariant().Contains("/") && !(num[i].ToString().ToLowerInvariant().Contains("-") && num[i].ToString().ToLowerInvariant().Contains("+")))
            {
                num[i] = solve(num[i]).ToString();
            }
            if (num[i].ToString().ToLowerInvariant() == "x")
            {
                if (num.ToString().ToLowerInvariant().Contains("x") && num.ToString().ToLowerInvariant().Contains("/") && !(num[i].ToString().ToLowerInvariant().Contains("-") && num[i].ToString().ToLowerInvariant().Contains("+")))
                {
                    num[i] = solve(num[i]).ToString();
                }
                else if (num.ToString().ToLowerInvariant().Contains("+") && num.ToString().ToLowerInvariant().Contains("-") && !(num[i].ToString().ToLowerInvariant().Contains("x") && num[i].ToString().ToLowerInvariant().Contains("-")))
                {
                    num[i + 1] += "'";
                    num[i - 2] += "'";
                }
            }
        }
        for (int i = 0; i < num.Count; i++)
        {
            if (num[i].ToString().ToLowerInvariant() == "/")
            {
                if (num.ToString().ToLowerInvariant().Contains("x") && num.ToString().ToLowerInvariant().Contains("/") && !(num[i].ToString().ToLowerInvariant().Contains("-") && num[i].ToString().ToLowerInvariant().Contains("+")))
                {
                    num[i + 1] += "'";
                    num[i - 2] += "'";
                }
                else if (num.ToString().ToLowerInvariant().Contains("+") && num.ToString().ToLowerInvariant().Contains("-") && !(num[i].ToString().ToLowerInvariant().Contains("x") && num[i].ToString().ToLowerInvariant().Contains("-")))
                {
                    num[i + 1] += "'";
                    num[i - 2] += "'";
                }
                else if (!(num[i + 2] == "^"))
                {
                    try
                    {
                        num[i + 1] += "'";
                        num[i - 2] += "'";
                    }
                    catch { }
                }
            }
        }
        for (int i = 0; i < num.Count; i++)
        {
            if (num[i].ToString().ToLowerInvariant() == "+")
            {
                if (num.ToString().ToLowerInvariant().Contains("x") && num.ToString().ToLowerInvariant().Contains("/") && !(num[i].ToString().ToLowerInvariant().Contains("-") && num[i].ToString().ToLowerInvariant().Contains("+")))
                {
                    num[i + 1] += "'";
                    num[i - 2] += "'";
                }
                else if (num.ToString().ToLowerInvariant().Contains("+") && num.ToString().ToLowerInvariant().Contains("-") && !(num[i].ToString().ToLowerInvariant().Contains("x") && num[i].ToString().ToLowerInvariant().Contains("-")))
                {
                    num[i + 1] += "'";
                    num[i - 2] += "'";
                }
                else
                {
                    try
                    {
                        num[i + 1] += "'";
                        num[i - 2] += "'";
                    }
                    catch { }
                }
            }
        }
        for (int i = 0; i < num.Count; i++)
        {
            if (num[i].ToString().ToLowerInvariant() == "-")
            {
                if (num.ToString().ToLowerInvariant().Contains("x") && num.ToString().ToLowerInvariant().Contains("/") && !(num[i].ToString().ToLowerInvariant().Contains("-") && num[i].ToString().ToLowerInvariant().Contains("+")))
                {
                    num[i + 1] += "'";
                    num[i - 2] += "'";
                }
                else if (num.ToString().ToLowerInvariant().Contains("+") && num.ToString().ToLowerInvariant().Contains("-") && !(num[i].ToString().ToLowerInvariant().Contains("x") && num[i].ToString().ToLowerInvariant().Contains("-")))
                {
                    num[i + 1] += "'";
                    num[i - 2] += "'";
                }
                else
                {
                    num[i + 1] += "'";
                    num[i - 2] += "'";
                }
            }
        }

        for (int i = 0; i < num.Count; i++)
        {
            eq += num[i];
        }
        String[] e = Regex.Split(eq, "'");
        List<string> newEq = new List<string>();
        for(int i = 0; i < e.Length; i++)
        {
            newEq.Add(e[i]);
        }
        /*for (int i = 0; i < newEq.Count; i++)
        {
            Console.WriteLine(newEq[i]);
        }*/
        for (int i = 0; i < newEq.Count; i++)
        {
            if (newEq[i].Contains("^"))
            {
                if (newEq[i][0] == '^')
                {
                    newEq[i] = newEq[i] + newEq[i - 1];
                    newEq.RemoveAt(i - 1);
                }
                else if (newEq[i][newEq[i].Length - 1] == '^')
                {
                    newEq[i] = newEq[i] + newEq[i + 1];
                    newEq.RemoveAt(i + 1);
                }
                newEq[i] = solve(newEq[i]).ToString();

            }
        }
        for (int i = 0; i < newEq.Count; i++)
        {
            if (newEq[i].Contains("x"))
            {
                if (newEq[i][0] == 'x')
                {
                    newEq[i] = newEq[i] + newEq[i - 1];
                    newEq.RemoveAt(i - 1);
                }
                else if (newEq[i][newEq[i].Length - 1] == 'x')
                {
                    newEq[i] = newEq[i] + newEq[i + 1];
                    
                    newEq.RemoveAt(i + 1);
                }
                newEq[i] = solve(newEq[i]).ToString();

            }
        }
        for (int i = 0; i < newEq.Count; i++)
        {
            try
            {
                if (newEq[i][0] == '/')
                {
                    newEq[i] = newEq[i] + newEq[i - 1];
                    newEq.RemoveAt(i - 1);
                }
                else if (newEq[i][newEq[i].Length - 1] == '/')
                {
                    newEq[i] = newEq[i] + newEq[i + 1];
                    newEq.RemoveAt(i + 1);
                }
                newEq[i] = solve(newEq[i]).ToString();

            }
            catch { }
                newEq[i] = solve(newEq[i]).ToString();
    }
        for (int i = 0; i < newEq.Count; i++)
        {
            if (newEq[i].Contains("+"))
            {
                newEq[i] = solve(newEq[i]).ToString();
            }
        }
        int a = 0;
        for (int i = 0; i < newEq.Count; i++)
        {
            if (newEq[i] == "-")
            {
                if (newEq[i + 1] == "")
                {
                    newEq[i] = newEq[i - 1] + newEq[i] + newEq[i + 2];
                    newEq.RemoveAt(i - 1);
                    newEq.RemoveAt(i + 1);
                    newEq[i] = solve(newEq[i]).ToString();
                }
                else
                {
                    newEq[i] = newEq[i - 1] + newEq[i] + newEq[i + 1];
                    newEq.RemoveAt(i - 1);
                    newEq.RemoveAt(i + 1);
                    newEq[i] = solve(newEq[i]).ToString();
                }
            }
        }
        for (int i = 0; i < newEq.Count; i++)
        {
            try
            {
                a += Int32.Parse(newEq[i]);
            }
            catch { }
        }
        for (int i = 0; i < newEq.Count; i++)
        {
            Console.WriteLine(newEq[i]);
        }
        Console.WriteLine(a);
    }
    public static void Main()
    {
        while (true)
        {
            splitItUp(Console.ReadLine());
        }
    }
}
