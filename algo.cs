using System;
public class program{
    static void solve(string equation)
    {
        int w = 0;
        double a = 0;
        if (equation[equation.Length - 1] == '-')
        {
            equation = "0" + equation + "0";
        }
        else
        {
            equation = "0+" + equation + "+0";
        }

        char[] mun = equation.ToCharArray();
        string[] num = new string[mun.Length];
        
        for (int i = 0; i < mun.Length; i++)
        {
            num[i] = mun[i].ToString().ToLowerInvariant();
        }
            for (int i = 0; i < num.Length; i++)
        {
            try
            {
                if (Int32.TryParse(num[i], out w) && Int32.TryParse(num[i + 1], out w))
                {
                    if (Int32.TryParse(num[i + 1], out w) && Int32.TryParse(num[i + 2], out w))
                    {
                        if (Int32.TryParse(num[i + 2], out w) && Int32.TryParse(num[i + 3], out w))
                        {
                            num[i] = num[i] + num[i + 1] + num[i + 2] + num[i + 3];
                            num[i + 1] = "";
                            num[i + 2] = "";
                            num[i + 3] = "";
                        }
                        else
                        {
                            num[i] = num[i] + num[i + 1] + num[i + 2];
                            num[i + 1] = "";
                            num[i + 2] = "";

                        }
                    }
                    else
                    {
                        num[i] = num[i] + num[i + 1];
                        num[i + 1] = "";
                    }
                    }
                }
            catch { }
        }
        for (int y = 0; y < num.Length; y++)
        {
            if (num[y] == "+" || (num[y] == "/") || (num[y] == "x") || (num[y] == "-") || (num[y] == "!"))
            {
                //do nothing and cry
            }
            else
            {
                try
                {
                    if (num[y - 1] == "!")
                    {
                        int fact = Int32.Parse(num[y]);
                        for (int i = fact - 1; i > 0; i--)
                        {
                            fact *= i;
                        }

                        a += fact;
                    }

                    if (num[y - 1] == "+")
                    {
                        a += Int32.Parse(num[y]);
                    }

                    if (num[y] == "^")
                    {
                        a = Math.Pow(a, Int32.Parse(num[y + 1]));
                    }

                    if (num[y - 1] == "x")
                    {
                        a *= Int32.Parse(num[y]);
                    }

                    if (num[y - 1] == "/")
                    {
                        a /= Int32.Parse(num[y]);
                    }

                    if (num[y - 1] == "-")
                    {
                        a -= Int32.Parse(num[y]);
                    }
                }
                catch
                {
                    try {
                        a += Int32.Parse(num[y]);
                    }
                    catch{}
                }
            }
        }
        Console.WriteLine(a);
    }
    public static void Main(String[] args)
    {
        while(true){
            solve(Console.ReadLine());
        }
    }
}