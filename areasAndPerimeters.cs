	static double perimeterCircle(int radius){
		double b = Math.PI*radius*2;
		return b;
	}
	static double areaSquare(int width,int height){
		return width*height;
	}
	static double perimeterSquare(int width,int height){
		return width*2+height*2;
	}
	static double areaTriangle(int Base, int height){
		return Base*height/2;
	}
	static double perimeterTriangle(int Base, int sideOne, int sideTwo){
		return Base+sideOne+sideTwo;
	}
		static double areaTrap(int baseOne, int baseTwo, int height){
		return (baseOne+baseTwo)/2*height;	
	}
	static double perimeterTrap(int baseOne, int baseTwo, int heightOne, int heightTwo){
		return baseOne + baseTwo + heightOne + heightTwo;
	}
	